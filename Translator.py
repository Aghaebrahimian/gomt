# -*- coding: utf-8 -*-
"""
Created on March 01 2019

@author: Ahmad Aghaebrahimian

Description: Given a json file containing source language sentences one per line, it first initialized a
dataframweork with source sentences and filled target fields with nones. Then it gradually fills Nones with
translations considering that frquency of API calls is limited
"""

import os
import sys
import time
import json
import random
import logging
import translate
import pandas as pd


logging.basicConfig(filename='GoMT.log', level=logging.INFO)
data_prefix = 'sample_data'


def progress_report(n, len_rows):
    """
    :param n: current iterator index
    :param len_rows: number of all items
    :return: print the progress bar
    """
    percent = 100 * n / len_rows
    line = '[{0}{1}]'.format('=' * int(percent / 2), ' ' * (50 - int(percent / 2)))
    status = '\r{0:3.0f}%{1} {2:3d}/{3:3d} processed!'
    sys.stdout.write(status.format(percent, line, n, len_rows))


def translator(jsn_path, source_langauge, target_langauge='en', min_complete=0.8):
    """
    :param list_of_sentences: list of sentences as string
    :param source_langauge: must be mentioned
    :return: gradulaly compete the dataframe associated with the provided json file
    """
    num_failure = 0
    num_tolerance = 3           # number of time it tries to translate one sentence
    max_tolerance = 30         # number pf total failure in one run after which the program is halted gracefully
    json_root = os.path.split(jsn_path)[:-1]
    json_name = os.path.split(jsn_path)[-1].split('.')[0]

    completed = check_status(sample_data)
    if completed>min_complete:
        print('Completion criteria is already satisfied')
        return
    else:
        print('The dataframe is %0.2f percent completed\n'%completed)
        dataframe = pd.read_pickle(os.path.join(*json_root, json_name+'_frame.pkl'))

        for n, sentence in enumerate(dataframe.values):
            progress_report(n, dataframe.shape[0])
            if sentence[1] is None:
                for tlr in range(num_tolerance):
                    try:
                        translation = translate.google(sentence[0], from_language=source_langauge, to_language=target_langauge)
                        sentence[1] = translation

                        logging.info('One sentence translated successfully!')
                        dataframe.to_pickle(os.path.join(*json_root, json_name+'_frame.pkl'))
                        time.sleep(random.randint(5, 15))
                        break
                    except Exception as e:
                        num_failure += 1
                        logging.info(str(e))
                        time.sleep(random.randint(30, 90))
            if num_failure > max_tolerance:
                print('Trnalation Engine is expriencing difficulties! Try later')
                sys.exit()
    return


def json2dataframe(jsn_path):
    """
    :param jsn_path: path of a json file contians sentences per line. json file should have .json suffix
    :return: dataframe with source and targets filled with sentences in the json and Nones respectively
    """
    json_root = os.path.split(jsn_path)[:-1]
    json_name = os.path.split(jsn_path)[-1].split('.')[0]
    dataframe_dict = {'source':[],'target':[]}
    sample_data = json.load(open(jsn_path))
    for sentence in sample_data:

        dataframe_dict['source'].append(sentence)
        dataframe_dict['target'].append(None)

    dataframe = pd.DataFrame(dataframe_dict)
    dataframe.to_pickle(os.path.join(*json_root, json_name+'_frame.pkl'))
    return


def check_status(jsn_path):
    """
    :param jsn_path: path of json file containing translations
    :return: how much of the file is translated in its corresponding dataframe
    """
    json_root = os.path.split(jsn_path)[:-1]
    json_name = os.path.split(jsn_path)[-1].split('.')[0]
    dataframe_name = os.path.join(*json_root, json_name+'_frame.pkl')
    if os.path.isfile(dataframe_name):
        dataframe = pd.read_pickle(dataframe_name)
        return sum(1 for x in dataframe.target if x is not None)/float(dataframe.shape[0])
    else:
        json2dataframe(jsn_path)
        return 0


# ==================================================  TEST CASE  ==================================================
if __name__=='__main__':
    data_prefix = 'sample_data'
    sample_data = os.path.join(data_prefix, 'samples_data_json.json')

    translator(sample_data,  source_langauge='de')
